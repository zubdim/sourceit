/**
 * Created by ZubDim on 06.04.14.
 */
public class Main {
    public static void main(String[] args) {

        hanoi_towers(7, 1, 3, 2);

    }

    static void hanoi_towers(int quantity, int from, int to, int buf_peg)
    //quantity-число колец, from-начальное положение колец(1-3),to-конечное положение колец(1-3)
    //buf_peg - промежуточный колышек(1-3)

    {
        if (quantity != 0)
        {
            hanoi_towers(quantity-1, from, buf_peg, to);

            System.out.println("quantity_" +quantity+ "from " + from + " to " + to + "--");

            hanoi_towers(quantity-1, buf_peg, to, from);
        }
    }
}
