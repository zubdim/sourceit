/**
 * Created by ZubDim on 06.04.14.
 */
public class Tower {
    private int[] Disks = new int[7];


    public int[] getDisksstatus(){
        return Disks;
    }

    public void setDisks(int[] disks) {
        Disks = (int[])disks.clone();
    }

    public boolean isEmptytower(){

        for (int i=0;i<Disks.length;i++){
            if (Disks[i]==0)
                continue;
            else
                return false;
        }
            return true;
    }

    private boolean isFullofDisksTower() {

        for (int i=0;i<Disks.length;i++){
            if (Disks[i]>0)
                continue;
            else
                return false;
        }
        return true;
    }

    public int getLastdiskTomove(){
        if (isEmptytower())
            return 0;

        for (int i=1;i<Disks.length;i++){
            if (Disks[i]==0)
                    return Disks[i-1];
        }
        return Disks[Disks.length-1];
    }

    public void deleteDiskafterItsmovement(){
        for (int i=1;i<Disks.length;i++){
            if (Disks[i]==0)
                Disks[i-1]=0;
        }
        Disks[Disks.length-1]=0;
    }


    public void puLastdisk(int diskWeightoPut){
        for (int i=0;i<Disks.length;i++){
            if (Disks[i]==0) {
                Disks[i]=diskWeightoPut;
                break;
            }
        }
    }


}
