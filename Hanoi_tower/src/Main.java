/**
 * Created by ZubDim on 06.04.14.
 */
public class Main {
private static int[] fullTowervalues = {7,6,5,4,3,2,1};
private static int[] emptyTowervalues = {0,0,0,0,0,0,0};
private static int counter = 0;




    public static void main(String[] args) {
        Tower t_1;
        Tower t_2;
        Tower t_3;

        t_1 = new Tower();
        t_2 = new Tower();
        t_3 = new Tower();

        t_1.setDisks(fullTowervalues);
        t_2.setDisks(emptyTowervalues);
        t_3.setDisks(emptyTowervalues);


        while (!resultIsreached(t_3.getDisksstatus())){

            if (getMovementavailability(t_1.getLastdiskTomove(), t_2.getLastdiskTomove())){
                t_2.puLastdisk(t_1.getLastdiskTomove());
                t_1.deleteDiskafterItsmovement();
                System.out.println("Step_1_2");
            }
            else if (getMovementavailability(t_1.getLastdiskTomove(), t_3.getLastdiskTomove())){
                t_3.puLastdisk(t_1.getLastdiskTomove());
                t_1.deleteDiskafterItsmovement();
                System.out.println("Step_1_3");
            }
            else if (getMovementavailability(t_2.getLastdiskTomove(), t_3.getLastdiskTomove())){
                t_3.puLastdisk(t_2.getLastdiskTomove());
                t_2.deleteDiskafterItsmovement();
                System.out.println("Step_2_3");
            }

            else if (getMovementavailability(t_2.getLastdiskTomove(), t_1.getLastdiskTomove())){
                t_1.puLastdisk(t_2.getLastdiskTomove());
                t_2.deleteDiskafterItsmovement();
                System.out.println("Step_2_1");
            }
            else if(getMovementavailability(t_3.getLastdiskTomove(), t_2.getLastdiskTomove())){
                t_2.puLastdisk(t_3.getLastdiskTomove());
                t_3.deleteDiskafterItsmovement();
                System.out.println("Step_3_2");
            }
            else if(getMovementavailability(t_3.getLastdiskTomove(), t_1.getLastdiskTomove())){
                t_1.puLastdisk(t_3.getLastdiskTomove());
                t_3.deleteDiskafterItsmovement();
                System.out.println("Step_3_1");
            }
            else
                System.out.println("Step_else");

            getCurrentstatus(t_1.getDisksstatus(),t_2.getDisksstatus(),t_3.getDisksstatus());
            counter++;
            if (counter==100)
                break;
        }
        //getCurrentstatus(t_1.getDisksstatus(), t_2.getDisksstatus(), t_3.getDisksstatus());
        sayGoodbye();
    }


    /*function which indicates our tower is
    full of disks so we can inform the User Program result is reached
     */
    public static boolean resultIsreached(int[] towerStatus){

        for (int i=1;i<towerStatus.length;i++){
            if (towerStatus[i]>towerStatus[i-1]){
                continue;
            } else {
            return false;
            }
        }
        return true;
    }

    /*
    function which show all the disks capacity for the User
    */
    public static void getCurrentstatus(int[] Tower_1,int[] Tower_2,int[] Tower_3){
        String Tower_1_line="";
        String Tower_2_line="";
        String Tower_3_line="";

        for (int i=0;i<Tower_1.length;i++){
            Tower_1_line +=" [" + Tower_1[i] + "]";
            Tower_2_line +=" [" + Tower_2[i] + "]";
            Tower_3_line +=" [" + Tower_3[i] + "]";
        }
        System.out.println(Tower_1_line);
        System.out.println(Tower_2_line);
        System.out.println(Tower_3_line);


    }
    /*
    function which greets the User before Progream exits
    */
    public static void sayGoodbye(){
        System.out.println("Goodbye!");
    }

    public static boolean getMovementavailability(int diskWeightFrom, int diskWeightTo){
        if (diskWeightFrom==0)
            return false;
        else if (diskWeightTo==0)
            return true;
        else if (diskWeightTo>diskWeightFrom)
            return true;
        else
            return false;
    }



}
