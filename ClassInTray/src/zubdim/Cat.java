package zubdim;

/**
 * Created by ZubDim on 26.03.14.
 */
public class Cat {
    static int quantity_of_objects;

    static private String color;
    static private String type;
    static private int size;

    static final String GREETINGS_WORD = "Hello, i'm a cat:";
    static final String MY_COLOR = "my color is:";
    static final String MY_TYPE = "my type is:";
    static final String MY_SIZE = "my size is:";

    public Cat(String color, String type, int size){
        objectIncrement();

        this.color = color;
        this.type = type;
        this.size = size;
    }

    public Cat(){
        objectIncrement();

        this.color = "Brown";
        this.type = "pusssy";
        this.size = 10;
    }

    private void objectIncrement(){
        quantity_of_objects++;
    }

    public int getQuantity_of_objects(){
        return quantity_of_objects;
    }

    public String sayHello() {
        return (GREETINGS_WORD + MY_COLOR + color + "," + MY_TYPE + type + "," + MY_SIZE + size );

    }
}
