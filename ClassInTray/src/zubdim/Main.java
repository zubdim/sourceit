package zubdim;

/**
 * Created by ZubDim on 26.03.14.
 */
public class Main {

    public static void main (String[] args){

            Cat street_cat = new Cat();

        System.out.println(street_cat.sayHello());
        System.out.println("Quantity of objects created:" + Cat.quantity_of_objects);

            Cat glamour_cat = new Cat ("Grey","Pushistik",5);

        System.out.println(glamour_cat.sayHello());
        System.out.println("Quantity of objects created:" + Cat.quantity_of_objects);

        Cat another_cat = new Cat ("White","Belyash",15);

        System.out.println(another_cat.sayHello());
        System.out.println("Quantity of objects created:" + Cat.quantity_of_objects);
        //Вот здесь вставляем комментарий и посмотрим, что быдет отображаться в Гите



    }

}
